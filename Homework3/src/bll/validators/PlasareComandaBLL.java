package bll.validators;

import java.util.ArrayList;
import java.util.List;

import dao.PlasareComandaDAO;
import model.Comanda;

public class PlasareComandaBLL {

	private static List<Validator<Comanda>> validators;

	public PlasareComandaBLL() {
		validators = new ArrayList<Validator<Comanda>>();
	}

	public int insertComanda(Comanda comanda) {
		for (Validator<Comanda> v : validators) {
			v.validate(comanda);
		}
		return PlasareComandaDAO.insert(comanda);
	}

}
