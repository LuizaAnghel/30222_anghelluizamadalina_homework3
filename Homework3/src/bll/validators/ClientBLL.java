package bll.validators;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import dao.ClientDAO;
import model.Client;

public class ClientBLL {

	private static List<Validator<Client>> validators;

	public ClientBLL() {
		validators = new ArrayList<Validator<Client>>();
		validators.add(new EmailValidator());
	}

	public Client findClientById(int Id_Client) {
		Client st = ClientDAO.findById(Id_Client);
		if (st == null) {
			throw new NoSuchElementException("The client with id =" + Id_Client + " was not found!");
		}
		return st;
	}

	public int insertClient(Client client) {
		for (Validator<Client> v : validators) {
			v.validate(client);
		}
		return ClientDAO.insert(client);
	}

	public void deleteClient(int Id_Client) {
		ClientDAO.delete(Id_Client);
	}

	public ArrayList<Client> afisareClient(ArrayList<Client> c) {

		return ClientDAO.afisareClient(c);
	}
	
	public void updateClient(Client client, int Id_Client) {
		ClientDAO.update(client, Id_Client);
	}
}
