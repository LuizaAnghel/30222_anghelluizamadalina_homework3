package bll.validators;

import java.util.ArrayList;
import java.util.List;
import dao.ProdusDAO;

import model.Produs;

public class ProdusBLL {

	private static List<Validator<Produs>> validators;

	public ProdusBLL() {
		validators = new ArrayList<Validator<Produs>>();
	}

	// public Client findClientById(int Id_Client) {
	// Client st = ClientDAO.findById(Id_Client);
	// if (st == null) {
	// throw new NoSuchElementException("The client with id =" + Id_Client + "
	// was not found!");
	// }
	// return st;
	// }

	public int insertProdus(Produs produs) {
		for (Validator<Produs> v : validators) {
			v.validate(produs);
		}
		return ProdusDAO.insert(produs);
	}

	public void deleteProdus(int Id_Produs) {
		ProdusDAO.delete(Id_Produs);
	}

	public void selectProdus(int Id_Produs, int Cant) {
		ProdusDAO.selectProdus(Id_Produs, Cant);
	}
	
	public void updateProdus(Produs produs, int Id_Client) {
		ProdusDAO.update(produs, Id_Client);
	}
	
	public int cantitateProdus(int Id_Produs) {
		return ProdusDAO.cantitateProdus(Id_Produs);
	}
	
	public double pretProdus(int Id_Produs) {
		return ProdusDAO.pretProdus(Id_Produs);
	}
	

}
