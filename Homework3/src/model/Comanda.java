package model;

public class Comanda {
	
	private static int Id_Comanda;
	private  static int Id_Client;
	private static int Id_Produs;
	private static int Cantitate;
	private static Double Suma_Finala;
	
	public Comanda(int Id_Client, int Id_Produs, int Cantitate, Double Suma_Finala) {
		//this.Id_Comanda = Id_Comanda;
		this.Id_Client = Id_Client;
		this.Id_Produs = Id_Produs;
		this.Cantitate = Cantitate;
		this.Suma_Finala = Suma_Finala;
	}
	
	public static int getId_Comanda() {
		return Id_Comanda;
	}
	public static void setId_Comanda(int id_Comanda) {
		Id_Comanda = id_Comanda;
	}
	public static int getId_Client() {
		return Id_Client;
	}
	public static void setId_Client(int id_Client) {
		Id_Client = id_Client;
	}
	public static int getId_Produs() {
		return Id_Produs;
	}
	public static void setId_Produs(int id_Produs) {
		Id_Produs = id_Produs;
	}
	public static int getCantitate() {
		return Cantitate;
	}
	public static void setCantitate(int cantitate) {
		Cantitate = cantitate;
	}
	public static Double getSuma_Finala() {
		return Suma_Finala;
	}
	public static void setSuma_Finala(Double suma_Finala) {
		Suma_Finala = suma_Finala;
	}

}
