package model;

public class Produs {
	
	private static int Id_Produs;
	private  static String Denumire;
	private static Double Pret;
	private static int Cantitate;
	
	public Produs(int Id_Produs, String Denumire, Double Pret, int Cantitate) {
		Produs.Id_Produs = Id_Produs;
		Produs.Denumire = Denumire;
		Produs.Pret = Pret;
		Produs.Cantitate = Cantitate;
	}

	public static int getId_Produs() {
		return Id_Produs;
	}

	public static void setId_Produs(int id_Produs) {
		Id_Produs = id_Produs;
	}

	public static String getDenumire() {
		return Denumire;
	}

	public static void setDenumire(String denumire) {
		Denumire = denumire;
	}

	public static Double getPret() {
		return Pret;
	}

	public static void setPret(Double pret) {
		Pret = pret;
	}

	public static int getCantitate() {
		return Cantitate;
	}

	public static void setCantitate(int cantitate) {
		Cantitate = cantitate;
	}
	
	@Override
	public String toString() {
		return "Produs [id=" + Id_Produs + ", denumire=" + Denumire + ", pret=" + Pret + ", cantitate=" + Cantitate 
				+ "]";
	}
	

}
