package model;

public class Client {
	
	private static int Id_Client;
	private  static String Nume;
	private static String Adresa;
	private static String Email;
	
	
	public Client(int Id_Client, String Nume, String Adresa, String Email) {
		Client.Id_Client = Id_Client;
		Client.Nume = Nume;
		Client.Adresa = Adresa;
		Client.Email = Email;
	}
	
	public int getId_Client() {
		return Id_Client;
	}

	public void setId_Client(int id_Client) {
		Id_Client = id_Client;
	} 

	public String getNume() {
		return Nume;
	}

	public void setNume(String nume) {
		Nume = nume;
	}

	public String getAdresa() {
		return Adresa;
	}

	public void setAdresa(String adresa) {
		Adresa = adresa;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	@Override
	public String toString() {
		return "Client [id=" + Id_Client + ", nume=" + Nume + ", adresa=" + Adresa + ", email=" + Email 
				+ "]";
	}

}
