package presentation;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;

import bll.validators.ProdusBLL;
import connection.ConnectionFactory;
import model.Produs;

public class ProdusView {

	static JFrame frame = new JFrame();
	JPanel panel = new JPanel();
	public String denumire;
	public int id, can;
	public Double pr;

	public void optiuneProdus() {
		frame = new JFrame("Produs");
		panel = new JPanel();
		panel.setLayout(null);
		frame.setSize(400, 400);
		panel.setBounds(1, 1, 399, 399);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);

		Font font = new Font("arial", Font.BOLD, 20);

		JLabel l = new JLabel("PRODUS");
		l.setFont(font);
		l.setBounds(70, 2, 100, 50);
		panel.add(l);

		JButton back = new JButton("BACK");
		back.setFont(font);
		back.setBounds(70, 310, 100, 50);
		panel.add(back);
		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
				View view = new View();
				view.createView();
			}

		});

		Font font2 = new Font("arial", Font.BOLD, 15);

		JButton b1 = new JButton("Vizualizare Produse");
		b1.setFont(font2);
		b1.setBounds(10, 50, 200, 50);
		panel.add(b1);

		b1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				frame.setVisible(false);
				JFrame ff = new JFrame("Lista Produse");

				JPanel p = new JPanel();
				p.setLayout(null);
				ff.setSize(400, 400);
				p.setBounds(1, 1, 399, 399);
				ff.setResizable(false);
				ff.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				ff.setLocationRelativeTo(null);

				Connection dbConnection = ConnectionFactory.getConnection();
				ArrayList<Produs> array = new ArrayList<Produs>();

				PreparedStatement afisareStatement = null;
				ResultSet rs = null;
				Produs produs = null;
				try {

					afisareStatement = dbConnection.prepareStatement("SELECT * FROM Produs");

					rs = afisareStatement.executeQuery();
					int row = 0;

					Object[][] data = new Object[20][4];
					String[] column_names = { "Id_Produs", "Denumire", "Pret", "Cantitate" };
					while (rs.next()) {
						row++;
						int Id_Produs = rs.getInt("Id_Produs");
						String Denumire = rs.getString("Denumire");
						Double Pret = rs.getDouble("Pret");
						int Cantitate = rs.getInt("Cantitate");

						produs = new Produs(Id_Produs, Denumire, Pret, Cantitate);
						// System.out.println(cl.getId_Client()+cl.getNume());

						data[0][0] = "Id_Produs";
						data[0][1] = "Denumire";
						data[0][2] = "Pret";
						data[0][3] = "Cantitate";

						data[row][0] = Id_Produs;
						data[row][1] = Denumire;
						data[row][2] = Pret;
						data[row][3] = Cantitate;

						JTable table = new JTable(data, column_names);
						table.setBounds(10, 10, 400, 400);

						p.add(table);
						array.add(produs);

					}
					row--;
				} catch (SQLException e) {
					// LOGGER.log(Level.WARNING, "ClientDAO:afisareClient " +
					// e.getMessage());
				} finally {
					ConnectionFactory.close(afisareStatement);
					ConnectionFactory.close(dbConnection);
					ConnectionFactory.close(rs);
				}

				JButton back = new JButton("BACK");
				back.setFont(font);
				back.setBounds(120, 330, 90, 40);
				ff.add(back);
				back.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						ff.setVisible(false);
						frame.setVisible(true);
					}

				});

				ff.add(p);
				ff.setVisible(true);
			}
		});

		JButton b2 = new JButton("Adaugare Produs");
		b2.setFont(font2);
		b2.setBounds(10, 120, 200, 50);
		panel.add(b2);

		b2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
				adaugareProdus();
			}

		});

		JButton b3 = new JButton("Stergere Produs");
		b3.setFont(font2);
		b3.setBounds(10, 190, 200, 50);
		panel.add(b3);
		b3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
				stergeProdus();

			}

		});
		JButton b4 = new JButton("Editare Produs");
		b4.setFont(font2);
		b4.setBounds(10, 260, 200, 50);
		panel.add(b4);
		b4.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				editareProdus();
			}
			
		});

		frame.add(panel);
		frame.setVisible(true);

	}

	public void adaugareProdus() {
		JFrame frame4 = new JFrame("Produs");
		JPanel panel4 = new JPanel();
		panel4.setLayout(null);
		frame4.setSize(400, 400);
		panel4.setBounds(1, 1, 399, 399);
		frame4.setResizable(false);
		frame4.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame4.setLocationRelativeTo(null);

		Font font = new Font("arial", Font.BOLD, 20);
		JLabel label = new JLabel("Adauga Produs");
		label.setFont(font);
		label.setBounds(5, 30, 200, 50);
		panel4.add(label);

		Font font2 = new Font("arial", Font.BOLD, 15);

		// JLabel label2 = new JLabel("Id_Produs");
		// label2.setBounds(10, 70, 100, 100);
		// label2.setFont(font2);
		// panel4.add(label2);
		//
		// JTextField text1 = new JTextField();
		// text1.setBounds(90, 110, 30, 20);
		// panel4.add(text1);

		JLabel label3 = new JLabel("Denumire");
		label3.setBounds(10, 110, 100, 100);
		label3.setFont(font2);
		panel4.add(label3);

		JTextField text2 = new JTextField();
		text2.setBounds(90, 150, 100, 20);
		panel4.add(text2);

		JLabel label4 = new JLabel("Pret");
		label4.setBounds(10, 150, 100, 100);
		label4.setFont(font2);
		panel4.add(label4);

		JTextField text3 = new JTextField();
		text3.setBounds(90, 190, 100, 20);
		panel4.add(text3);

		JLabel label5 = new JLabel("Cantitate");
		label5.setBounds(10, 190, 100, 100);
		label5.setFont(font2);
		panel4.add(label5);

		JTextField text4 = new JTextField();
		text4.setBounds(90, 230, 100, 20);
		panel4.add(text4);

		JButton buton = new JButton("OK!");
		buton.setFont(font);
		buton.setBounds(270, 270, 100, 50);
		panel4.add(buton);
		buton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String id_produs, pret, cantitate_produs;
				// id_produs = text1.getText();
				// id = Integer.parseInt(id_produs);
				denumire = text2.getText();
				pret = text3.getText();
				pr = Double.parseDouble(pret);
				cantitate_produs = text4.getText();
				can = Integer.parseInt(cantitate_produs);

				ProdusBLL bll = new ProdusBLL();
				Produs produs = new Produs(id, denumire, pr, can);
				bll.insertProdus(produs);
			}

		});

		JButton back = new JButton("BACK");
		back.setFont(font);
		back.setBounds(90, 280, 100, 50);
		panel4.add(back);
		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame4.setVisible(false);
				frame.setVisible(true);
			}

		});

		frame4.add(panel4);
		frame4.setVisible(true);
	}

	public void stergeProdus() {
		JFrame frame5 = new JFrame("Produs");
		JPanel panel5 = new JPanel();
		panel5.setLayout(null);
		frame5.setSize(400, 400);
		panel5.setBounds(1, 1, 399, 399);
		frame5.setResizable(false);
		frame5.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame5.setLocationRelativeTo(null);

		Font font = new Font("arial", Font.BOLD, 20);

		JButton ok = new JButton("OK!");
		ok.setFont(font);
		ok.setBounds(270, 270, 100, 50);
		panel5.add(ok);

		JLabel lab = new JLabel("STERGE PRODUS!");
		lab.setFont(font);
		lab.setBounds(20, 20, 250, 50);
		panel5.add(lab);

		Font font2 = new Font("arial", Font.BOLD, 15);

		JLabel lab2 = new JLabel("Introdu id-ul produsului: ");
		lab2.setFont(font2);
		lab2.setBounds(50, 90, 250, 30);
		panel5.add(lab2);

		JTextField t = new JTextField();
		t.setBounds(50, 120, 30, 20);
		panel5.add(t);

		ok.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String text = t.getText();
				int id = Integer.parseInt(text);

				ProdusBLL bll = new ProdusBLL();
				bll.deleteProdus(id);

			}

		});

		JButton back = new JButton("BACK");
		back.setFont(font);
		back.setBounds(90, 280, 100, 50);
		panel5.add(back);
		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame5.setVisible(false);
				frame.setVisible(true);
			}

		});

		frame5.add(panel5);
		frame5.setVisible(true);
	}
	
	public static void editareProdus() {
		JFrame frame6 = new JFrame("Editare Client");
		JPanel panel6 = new JPanel();
		panel6.setLayout(null);
		frame6.setSize(400, 400);
		panel6.setBounds(1, 1, 399, 399);
		frame6.setResizable(false);
		frame6.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame6.setLocationRelativeTo(null);

		Font font = new Font("arial", Font.BOLD, 20);

		JButton ok = new JButton("OK!");
		ok.setFont(font);
		ok.setBounds(270, 270, 100, 50);
		panel6.add(ok);

		JLabel lab = new JLabel("EDITEAZA PRODUS!");
		lab.setFont(font);
		lab.setBounds(20, 20, 250, 50);
		panel6.add(lab);

		Font font2 = new Font("arial", Font.BOLD, 15);

		JLabel lab2 = new JLabel("Introdu id-ul produsului pe care vrei sa-l editezi: ");
		lab2.setFont(font2);
		lab2.setBounds(10, 90, 360, 30);
		panel6.add(lab2);

		JTextField t = new JTextField();
		t.setBounds(50, 120, 30, 20);
		panel6.add(t);
		

		JLabel label3 = new JLabel("Denumire");
		label3.setBounds(10, 110, 100, 100);
		label3.setFont(font2);
		panel6.add(label3);

		JTextField text2 = new JTextField();
		text2.setBounds(90, 150, 100, 20);
		panel6.add(text2);

		JLabel label4 = new JLabel("Pret");
		label4.setBounds(10, 150, 100, 100);
		label4.setFont(font2);
		panel6.add(label4);

		JTextField text3 = new JTextField();
		text3.setBounds(90, 190, 100, 20);
		panel6.add(text3);

		JLabel label5 = new JLabel("Cantitate");
		label5.setBounds(10, 190, 100, 100);
		label5.setFont(font2);
		panel6.add(label5);

		JTextField text4 = new JTextField();
		text4.setBounds(90, 230, 100, 20);
		panel6.add(text4);

		ok.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String text = t.getText();
				int id = Integer.parseInt(text);
//
				ProdusBLL bll = new ProdusBLL();
				
//				clientBll.deleteClient(id);
				String Denumire = text2.getText();
				String p = text3.getText();
				Double Pret = Double.parseDouble(p);
				String c= text4.getText();
				int Cantitate = Integer.parseInt(c);
				Produs produs = new Produs(id, Denumire,Pret,Cantitate);
				bll.updateProdus(produs, id);

			}

		});
		

		JButton back = new JButton("BACK");
		back.setFont(font);
		back.setBounds(90, 280, 100, 50);
		panel6.add(back);
		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame6.setVisible(false);
				frame.setVisible(true);
			}

		});

		frame6.add(panel6);
		frame6.setVisible(true);
	}


}
