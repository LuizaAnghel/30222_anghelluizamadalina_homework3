package presentation;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import javax.swing.JTable;
import javax.swing.JTextField;

import bll.validators.ClientBLL;
import connection.ConnectionFactory;
import model.Client;

public class View extends JFrame {

	/**
	 * 
	 */
	// private static final long serialVersionUID = 1L;
//	protected static final int MAX = 0;
	public JFrame frame, frame2;
	public static JFrame frame3;
	private JPanel panel, panel3;
	private JLabel label;
	private JButton buton1, buton2, buton3;
	// private JLabel l1, l2, l3, l4, l5, l6;
	// private JTextField t1, t2, t3, t4, t5;
	private JButton b1, b2, b3,b4;

	public void createView() {
		frame = new JFrame("Warehouse");
		panel = new JPanel();
		panel.setLayout(null);
		frame.setSize(400, 400);
		panel.setBounds(1, 1, 399, 399);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);

		Font font = new Font("arial", Font.BOLD, 20);

		label = new JLabel("WELCOME TO WAREHOUSE");
		label.setFont(font);
		label.setBounds(40, 20, 350, 50);
		panel.add(label);

		Font font2 = new Font("arial", Font.BOLD, 15);

		buton1 = new JButton("Plaseaza o comanda");
		buton1.setFont(font2);
		buton1.setBounds(80, 100, 220, 50);
		panel.add(buton1);

		buton2 = new JButton("Optiune client");
		buton2.setFont(font2);
		buton2.setBounds(20, 200, 140, 50);
		panel.add(buton2);

		buton3 = new JButton("Optiune produs");
		buton3.setFont(font2);
		buton3.setBounds(180, 200, 160, 50);
		panel.add(buton3);

		buton1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
				PlasareComandaView view = new PlasareComandaView();
				view.crearePlasareComanda();

			}

		});

		buton2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
				optiuneClient();

			}
		});

		buton3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);

				ProdusView produs = new ProdusView();
				produs.optiuneProdus();

			}

		});

		frame.add(panel);
		frame.setVisible(true);
	}

	public void optiuneClient() {

		frame3 = new JFrame("Client");
		panel3 = new JPanel();
		panel3.setLayout(null);
		frame3.setSize(400, 400);
		panel3.setBounds(1, 1, 399, 399);
		frame3.setResizable(false);
		frame3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame3.setLocationRelativeTo(null);

		Font font = new Font("arial", Font.BOLD, 20);

		JLabel l = new JLabel("CLIENT");
		l.setFont(font);
		l.setBounds(70, 2, 100, 50);
		panel3.add(l);

		JButton back = new JButton("BACK");
		back.setFont(font);
		back.setBounds(70, 310, 100, 50);
		panel3.add(back);
		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame3.setVisible(false);
				frame.setVisible(true);
			}

		});

		Font font2 = new Font("arial", Font.BOLD, 15);

		b1 = new JButton("Vizualizare Clienti");
		b1.setFont(font2);
		b1.setBounds(10, 50, 200, 50);
		panel3.add(b1);

		b1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				frame3.setVisible(false);
				JFrame ff = new JFrame("Lista Clienti");

				JPanel p = new JPanel();
				p.setLayout(null);
				ff.setSize(400, 400);
				p.setBounds(1, 1, 399, 399);
				ff.setResizable(false);
				ff.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				ff.setLocationRelativeTo(null);

				Connection dbConnection = ConnectionFactory.getConnection();
				ArrayList<Client> array = new ArrayList<Client>();

				PreparedStatement afisareStatement = null;
				ResultSet rs = null;
				Client cl = null;
				try {

					afisareStatement = dbConnection.prepareStatement("SELECT * FROM Client");

					rs = afisareStatement.executeQuery();
					int row = 0;

					Object[][] data = new Object[20][4];
					String[] column_names = { "Id_Client", "Nume", "Adresa", "Email" };
					while (rs.next()) {
						row++;
						int Id_Client = rs.getInt("Id_Client");
						String Nume = rs.getString("Nume");
						String Adresa = rs.getString("Adresa");
						String Email = rs.getString("Email");

						cl = new Client(Id_Client, Nume, Adresa, Email);
						// System.out.println(cl.getId_Client()+cl.getNume());

						data[0][0] = "Id_Client";
						data[0][1] = "Nume";
						data[0][2] = "Adresa";
						data[0][3] = "Email";

						data[row][0] = Id_Client;
						data[row][1] = Nume;
						data[row][2] = Adresa;
						data[row][3] = Email;

						JTable table = new JTable(data, column_names);
						table.setBounds(10, 10, 400, 400);

						p.add(table);
						array.add(cl);

					}
					row--;
				} catch (SQLException e) {
					// LOGGER.log(Level.WARNING, "ClientDAO:afisareClient " +
					// e.getMessage());
				} finally {
					ConnectionFactory.close(afisareStatement);
					ConnectionFactory.close(dbConnection);
					ConnectionFactory.close(rs);
				}

				JButton back = new JButton("BACK");
				back.setFont(font);
				back.setBounds(120, 330, 150, 40);
				ff.add(back);
				back.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						ff.setVisible(false);
						frame3.setVisible(true);
					}

				});

				ff.add(p);
				ff.setVisible(true);
			}
		});

		b2 = new JButton("Adaugare Client");
		b2.setFont(font2);
		b2.setBounds(10, 120, 200, 50);
		panel3.add(b2);

		b2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame3.setVisible(false);
				adaugareClient();
			}

		});

		b3 = new JButton("Stergere Client");
		b3.setFont(font2);
		b3.setBounds(10, 190, 200, 50);
		panel3.add(b3);
		b3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame3.setVisible(false);
				stergeClient();

			}

		});
		
		b4 = new JButton("Editare Client");
		b4.setFont(font2);
		b4.setBounds(10, 260, 200, 50);
		panel3.add(b4);
		b4.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame3.setVisible(false);
				editareClient();
				
			}
			
		});

		frame3.add(panel3);
		frame3.setVisible(true);

	}

	public void adaugareClient() {

		JFrame frame4 = new JFrame("Client");
		JPanel panel4 = new JPanel();
		panel4.setLayout(null);
		frame4.setSize(400, 400);
		panel4.setBounds(1, 1, 399, 399);
		frame4.setResizable(false);
		frame4.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame4.setLocationRelativeTo(null);

		Font font = new Font("arial", Font.BOLD, 20);
		JLabel label = new JLabel("Adauga Client");
		label.setFont(font);
		label.setBounds(5, 30, 200, 50);
		panel4.add(label);

		Font font2 = new Font("arial", Font.BOLD, 15);

		JLabel label2 = new JLabel("Id_Client");
		label2.setBounds(10, 70, 100, 100);
		label2.setFont(font2);
		panel4.add(label2);

		JTextField text1 = new JTextField();
		text1.setBounds(90, 110, 30, 20);
		panel4.add(text1);

		JLabel label3 = new JLabel("Nume");
		label3.setBounds(10, 110, 100, 100);
		label3.setFont(font2);
		panel4.add(label3);

		JTextField text2 = new JTextField();
		text2.setBounds(90, 150, 100, 20);
		panel4.add(text2);

		JLabel label4 = new JLabel("Adresa");
		label4.setBounds(10, 150, 100, 100);
		label4.setFont(font2);
		panel4.add(label4);

		JTextField text3 = new JTextField();
		text3.setBounds(90, 190, 100, 20);
		panel4.add(text3);

		JLabel label5 = new JLabel("Email");
		label5.setBounds(10, 190, 100, 100);
		label5.setFont(font2);
		panel4.add(label5);

		JTextField text4 = new JTextField();
		text4.setBounds(90, 230, 100, 20);
		panel4.add(text4);

		JButton buton = new JButton("OK!");
		buton.setFont(font);
		buton.setBounds(270, 270, 100, 50);
		panel4.add(buton);
		buton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String t1, nume, adresa, email;
				t1 = text1.getText();
				int id = Integer.parseInt(t1);
				nume = text2.getText();
				adresa = text3.getText();
				email = text4.getText();

				ClientBLL clientBll = new ClientBLL();
				Client client = new Client(id, nume, adresa, email);
				clientBll.insertClient(client);
			}

		});

		JButton back = new JButton("BACK");
		back.setFont(font);
		back.setBounds(90, 280, 100, 50);
		panel4.add(back);
		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame4.setVisible(false);
				frame3.setVisible(true);
			}

		});

		frame4.add(panel4);
		frame4.setVisible(true);

	}

	public void stergeClient() {
		JFrame frame5 = new JFrame("Client");
		JPanel panel5 = new JPanel();
		panel5.setLayout(null);
		frame5.setSize(400, 400);
		panel5.setBounds(1, 1, 399, 399);
		frame5.setResizable(false);
		frame5.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame5.setLocationRelativeTo(null);

		Font font = new Font("arial", Font.BOLD, 20);

		JButton ok = new JButton("OK!");
		ok.setFont(font);
		ok.setBounds(270, 270, 100, 50);
		panel5.add(ok);

		JLabel lab = new JLabel("STERGE CLIENT!");
		lab.setFont(font);
		lab.setBounds(20, 20, 250, 50);
		panel5.add(lab);

		Font font2 = new Font("arial", Font.BOLD, 15);

		JLabel lab2 = new JLabel("Introdu id-ul clientului: ");
		lab2.setFont(font2);
		lab2.setBounds(50, 90, 250, 30);
		panel5.add(lab2);

		JTextField t = new JTextField();
		t.setBounds(50, 120, 30, 20);
		panel5.add(t);

		ok.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String text = t.getText();
				int id = Integer.parseInt(text);

				ClientBLL clientBll = new ClientBLL();
				clientBll.deleteClient(id);

			}

		});

		JButton back = new JButton("BACK");
		back.setFont(font);
		back.setBounds(90, 280, 100, 50);
		panel5.add(back);
		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame5.setVisible(false);
				frame3.setVisible(true);
			}

		});

		frame5.add(panel5);
		frame5.setVisible(true);
	}
	
	public static void editareClient() {
		JFrame frame6 = new JFrame("Editare Client");
		JPanel panel6 = new JPanel();
		panel6.setLayout(null);
		frame6.setSize(400, 400);
		panel6.setBounds(1, 1, 399, 399);
		frame6.setResizable(false);
		frame6.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame6.setLocationRelativeTo(null);

		Font font = new Font("arial", Font.BOLD, 20);

		JButton ok = new JButton("OK!");
		ok.setFont(font);
		ok.setBounds(270, 270, 100, 50);
		panel6.add(ok);

		JLabel lab = new JLabel("EDITEAZA CLIENT!");
		lab.setFont(font);
		lab.setBounds(20, 20, 250, 50);
		panel6.add(lab);

		Font font2 = new Font("arial", Font.BOLD, 15);

		JLabel lab2 = new JLabel("Introdu id-ul clientului pe care vrei sa-l editezi: ");
		lab2.setFont(font2);
		lab2.setBounds(10, 90, 340, 30);
		panel6.add(lab2);

		JTextField t = new JTextField();
		t.setBounds(50, 120, 30, 20);
		panel6.add(t);
		

		JLabel label3 = new JLabel("Nume");
		label3.setBounds(10, 110, 100, 100);
		label3.setFont(font2);
		panel6.add(label3);

		JTextField text2 = new JTextField();
		text2.setBounds(90, 150, 100, 20);
		panel6.add(text2);

		JLabel label4 = new JLabel("Adresa");
		label4.setBounds(10, 150, 100, 100);
		label4.setFont(font2);
		panel6.add(label4);

		JTextField text3 = new JTextField();
		text3.setBounds(90, 190, 100, 20);
		panel6.add(text3);

		JLabel label5 = new JLabel("Email");
		label5.setBounds(10, 190, 100, 100);
		label5.setFont(font2);
		panel6.add(label5);

		JTextField text4 = new JTextField();
		text4.setBounds(90, 230, 100, 20);
		panel6.add(text4);

		ok.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String text = t.getText();
				int id = Integer.parseInt(text);
//
				ClientBLL clientBll = new ClientBLL();
				
//				clientBll.deleteClient(id);
				String Nume = text2.getText();
				String Adresa = text3.getText();
				String Email = text4.getText();
				Client client = new Client(id, Nume,Adresa,Email);
				clientBll.updateClient(client, id);

			}

		});
		

		JButton back = new JButton("BACK");
		back.setFont(font);
		back.setBounds(90, 280, 100, 50);
		panel6.add(back);
		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame6.setVisible(false);
				frame3.setVisible(true);
			}

		});

		frame6.add(panel6);
		frame6.setVisible(true);
	}

}
