package presentation;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import bll.validators.PlasareComandaBLL;
import bll.validators.ProdusBLL;
import model.Client;
import model.Comanda;
import model.Invoice;

public class PlasareComandaView {
	JFrame frame2;
	JPanel panel2;
	JLabel l1, l2, l3, l4, l5, l6;
	JTextField t1, t2, t3, t4, t5;
	public int id, id2, id3, can;

	public void crearePlasareComanda() {
		frame2 = new JFrame("Plasare Comanda");
		panel2 = new JPanel();
		panel2.setLayout(null);
		frame2.setSize(400, 400);
		panel2.setBounds(1, 1, 399, 399);
		frame2.setResizable(false);
		frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame2.setLocationRelativeTo(null);

		Font font = new Font("arial", Font.BOLD, 20);

		l1 = new JLabel("PLASEAZA O COMANDA!");
		l1.setBounds(60, 5, 300, 100);
		l1.setFont(font);
		panel2.add(l1);

		Font font2 = new Font("arial", Font.BOLD, 15);

		// l2 = new JLabel("Id_Comanda");
		// l2.setBounds(10, 70, 100, 100);
		// l2.setFont(font2);
		// panel2.add(l2);
		//
		// t1 = new JTextField();
		// t1.setBounds(110, 110, 30, 20);
		// panel2.add(t1);

		l3 = new JLabel("Id_Client");
		l3.setBounds(10, 110, 100, 100);
		l3.setFont(font2);
		panel2.add(l3);

		t2 = new JTextField();
		t2.setBounds(110, 150, 30, 20);
		panel2.add(t2);

		l4 = new JLabel("Id_Produs");
		l4.setBounds(10, 150, 100, 100);
		l4.setFont(font2);
		panel2.add(l4);

		t3 = new JTextField();
		t3.setBounds(110, 190, 30, 20);
		panel2.add(t3);

		l5 = new JLabel("Cantitate");
		l5.setBounds(10, 190, 100, 100);
		l5.setFont(font2);
		panel2.add(l5);

		t4 = new JTextField();
		t4.setBounds(110, 230, 30, 20);
		panel2.add(t4);

		JButton back = new JButton("BACK");
		back.setFont(font);
		back.setBounds(70, 310, 100, 50);
		panel2.add(back);
		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame2.setVisible(false);
				View view = new View();
				view.createView();
			}

		});

		JButton buton = new JButton("OK!");
		buton.setFont(font);
		buton.setBounds(270, 270, 100, 50);
		panel2.add(buton);

		buton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				//ProdusView produs2 = new ProdusView();

				String id_client, id_produs, cantitate;
				// id_comanda = text1.getText();
				// id = Integer.parseInt(id_comanda);
				id_client = t2.getText();
				id2 = Integer.parseInt(id_client);
				id_produs = t3.getText();
				id3 = Integer.parseInt(id_produs);
				cantitate = t4.getText();
				can = Integer.parseInt(cantitate);

				ProdusBLL prod = new ProdusBLL();
				int cant = prod.cantitateProdus(id3);
				System.out.println(cant);
				double pret = prod.pretProdus(id3);
				System.out.println(pret);
				
				if (can <= cant) {
					System.out.println(cant-can);
					PlasareComandaBLL bll = new PlasareComandaBLL();
					Comanda comanda = new Comanda(id2, id3, can,can*pret);
					System.out.println(can*pret);
					bll.insertComanda(comanda);
					ProdusBLL p = new ProdusBLL();
					p.selectProdus(id3, cant-can);
					Invoice invoice = new Invoice();
					invoice.createInvoice(id2, can * pret);

				}
				if (can > cant) {
					JOptionPane.showMessageDialog(null, "Nu sunt destule produse pe stoc!");
				}

//				 double suma_finala = can * produs2.pr;
//				 System.out.println(suma_finala);

			}

		});

		frame2.add(panel2);
		frame2.setVisible(true);

	}

	// public void adaugaComanda() {
	// JFrame frame4 = new JFrame("Comanda");
	// JPanel panel4 = new JPanel();
	// panel4.setLayout(null);
	// frame4.setSize(400, 400);
	// panel4.setBounds(1, 1, 399, 399);
	// frame4.setResizable(false);
	// frame4.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	// frame4.setLocationRelativeTo(null);
	//
	// Font font = new Font("arial", Font.BOLD, 20);
	// JLabel label = new JLabel("Adauga Comanda");
	// label.setFont(font);
	// label.setBounds(5, 30, 200, 50);
	// panel4.add(label);
	//
	// Font font2 = new Font("arial", Font.BOLD, 15);
	//
	//// JLabel label2 = new JLabel("Id_Comanda");
	//// label2.setBounds(10, 70, 100, 100);
	//// label2.setFont(font2);
	//// panel4.add(label2);
	////
	//// JTextField text1 = new JTextField();
	//// text1.setBounds(90, 110, 30, 20);
	//// panel4.add(text1);
	//
	// JLabel label3 = new JLabel("Id_Client");
	// label3.setBounds(10, 110, 100, 100);
	// label3.setFont(font2);
	// panel4.add(label3);
	//
	// JTextField text2 = new JTextField();
	// text2.setBounds(90, 150, 100, 20);
	// panel4.add(text2);
	//
	// JLabel label4 = new JLabel("Id_Produs");
	// label4.setBounds(10, 150, 100, 100);
	// label4.setFont(font2);
	// panel4.add(label4);
	//
	// JTextField text3 = new JTextField();
	// text3.setBounds(90, 190, 100, 20);
	// panel4.add(text3);
	//
	// JLabel label5 = new JLabel("Cantitate");
	// label5.setBounds(10, 190, 100, 100);
	// label5.setFont(font2);
	// panel4.add(label5);
	//
	// JTextField text4 = new JTextField();
	// text4.setBounds(90, 230, 100, 20);
	// panel4.add(text4);
	//
	// JButton buton = new JButton("OK!");
	// buton.setFont(font);
	// buton.setBounds(270, 270, 100, 50);
	// panel4.add(buton);
	// buton.addActionListener(new ActionListener() {
	//
	// @Override
	// public void actionPerformed(ActionEvent arg0) {
	// String id_client, id_produs, cantitate;
	//// id_comanda = text1.getText();
	//// id = Integer.parseInt(id_comanda);
	// id_client = text2.getText();
	// id2 = Integer.parseInt(id_client);
	// id_produs = text3.getText();
	// id3 = Integer.parseInt(id_produs);
	// cantitate = text4.getText();
	// can = Integer.parseInt(cantitate);
	//
	// PlasareComandaBLL bll = new PlasareComandaBLL();
	// Comanda comanda = new Comanda(id2, id3, can);
	// bll.insertComanda(comanda);
	////
	//// Connection dbConnection = ConnectionFactory.getConnection();
	////
	//// ResultSet rs = null;
	//// Produs produs = null;
	//// PreparedStatement afisareStatement = null;
	////
	//// try {
	////
	//// afisareStatement = dbConnection
	//// .prepareStatement("SELECT cantitate FROM Produs WHERE Id_Produs = ?");
	//// rs = afisareStatement.executeQuery();
	////
	//// } catch (SQLException e) {
	//// // LOGGER.log(Level.WARNING, "ClientDAO:afisareClient " +
	//// // e.getMessage());
	//// } finally {
	//// ConnectionFactory.close(afisareStatement);
	//// ConnectionFactory.close(dbConnection);
	//// ConnectionFactory.close(rs);
	//// }
	////
	//// ProdusView produs2 = new ProdusView();
	////
	//// if (can > produs2.can) {
	//// JOptionPane.showMessageDialog(null, "Nu sunt destule produse pe
	// stoc!");
	//// }
	//
	// }
	//
	// });
	//
	// JButton back = new JButton("BACK");
	// back.setFont(font);
	// back.setBounds(90, 280, 100, 50);
	// panel4.add(back);
	// back.addActionListener(new ActionListener() {
	//
	// @Override
	// public void actionPerformed(ActionEvent arg0) {
	// frame4.setVisible(false);
	// frame2.setVisible(true);
	// }
	//
	// });
	//
	// frame4.add(panel4);
	// frame4.setVisible(true);
	// }

}
