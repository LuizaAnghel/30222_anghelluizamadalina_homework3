package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Comanda;

public class PlasareComandaDAO {
	
	protected static final Logger LOGGER = Logger.getLogger(PlasareComandaDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO Comanda ( Id_Client, Id_Produs, Cantitate, Suma_Finala)"
			+ " VALUES (?,?,?,?)";
	
	public static int insert(Comanda comanda) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
		//	insertStatement.setInt(1, comanda.getId_Comanda());
			insertStatement.setInt(1, comanda.getId_Client());
			insertStatement.setInt(2, comanda.getId_Produs());
			insertStatement.setInt(3, comanda.getCantitate());
			insertStatement.setDouble(4, comanda.getSuma_Finala());

			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ComandaDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}

}
