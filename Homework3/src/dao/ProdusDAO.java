package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import bll.validators.ProdusBLL;
import connection.ConnectionFactory;
import model.Produs;

public class ProdusDAO {

	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO Produs (Id_Produs,Denumire,Pret,Cantitate)"
			+ " VALUES (?,?,?,?)";

	// private final static String afisStatementString = "SELECT * FROM Produs";
	private static final String deleteStatementString = "DELETE FROM Produs where Id_Produs = ?";
	private static final String updateStatementString = "UPDATE Produs SET Denumire = ?, Pret = ?, Cantitate = ? WHERE Id_Produs = ?";

	public static void update(Produs produs, int Id_Produs) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setString(1, produs.getDenumire());
			updateStatement.setDouble(2, produs.getPret());
			updateStatement.setInt(3, produs.getCantitate());
			updateStatement.setInt(4, Id_Produs);
			updateStatement.executeUpdate();

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}

	}

	public static int insert(Produs produs) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, produs.getId_Produs());
			insertStatement.setString(2, produs.getDenumire());
			insertStatement.setDouble(3, produs.getPret());
			insertStatement.setInt(4, produs.getCantitate());

			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}

	public static void delete(int Id_Produs) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;

		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, Id_Produs);

			deleteStatement.executeUpdate();

			// ResultSet rs = deleteStatement.getGeneratedKeys();

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}

	}

	public static void selectProdus(int Id_Produs, int Cant) {

		Connection dbConnection = ConnectionFactory.getConnection();

		ResultSet rs = null;
		// Produs produs = null;
		PreparedStatement afisareStatement = null;

		try {

			afisareStatement = dbConnection.prepareStatement("SELECT * FROM Produs WHERE Id_Produs = ?");
			afisareStatement.setInt(1, Id_Produs);
			rs = afisareStatement.executeQuery();
			rs.next();
			String denumire = rs.getString("Denumire");
			Double pret = rs.getDouble("Pret");
			int cantitate = rs.getInt("Cantitate");
			Produs produs = new Produs(Id_Produs, denumire, pret, Cant);
			ProdusBLL bll = new ProdusBLL();
			bll.updateProdus(produs,Id_Produs);

		} catch (SQLException e) {
			// LOGGER.log(Level.WARNING, "ClientDAO:afisareClient " +
			// e.getMessage());
		} finally {
			ConnectionFactory.close(afisareStatement);
			ConnectionFactory.close(dbConnection);
			ConnectionFactory.close(rs);
		}
	}

	public static int cantitateProdus(int Id_Produs) {
		Connection dbConnection = ConnectionFactory.getConnection();
		ResultSet rs = null;
		PreparedStatement cantitateStatement = null;
		int cantitate = 0;

		try {

			cantitateStatement = dbConnection.prepareStatement("SELECT Cantitate FROM Produs WHERE Id_Produs = ?");
			cantitateStatement.setInt(1, Id_Produs);
			rs = cantitateStatement.executeQuery();
			rs.next();
			// String denumire = rs.getString("Denumire");
			// Double pret = rs.getDouble("Pret");
			cantitate = rs.getInt("Cantitate");

		} catch (SQLException e) {
			// LOGGER.log(Level.WARNING, "ClientDAO:afisareClient " +
			// e.getMessage());
		} finally {
			ConnectionFactory.close(cantitateStatement);
			ConnectionFactory.close(dbConnection);
			ConnectionFactory.close(rs);
		}
		return cantitate;
	}

	public static double pretProdus(int Id_Produs) {
		Connection dbConnection = ConnectionFactory.getConnection();
		ResultSet rs = null;
		PreparedStatement cantitateStatement = null;
		double pret = 0;

		try {

			cantitateStatement = dbConnection.prepareStatement("SELECT Pret FROM Produs WHERE Id_Produs = ?");
			cantitateStatement.setInt(1, Id_Produs);
			rs = cantitateStatement.executeQuery();
			rs.next();
			// String denumire = rs.getString("Denumire");
			// Double pret = rs.getDouble("Pret");
			pret = rs.getDouble("Pret");

		} catch (SQLException e) {
			// LOGGER.log(Level.WARNING, "ClientDAO:afisareClient " +
			// e.getMessage());
		} finally {
			ConnectionFactory.close(cantitateStatement);
			ConnectionFactory.close(dbConnection);
			ConnectionFactory.close(rs);
		}
		return pret;
	}

}
