package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Client;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class ClientDAO {

	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO Client (Id_Client,Nume,Adresa,Email)"
			+ " VALUES (?,?,?,?)";
	private final static String findStatementString = "SELECT * FROM Client where Id_Client = ?";

	private final static String afisStatementString = "SELECT * FROM Client";
	private static final String deleteStatementString = "DELETE FROM Client where Id_Client = ?";
	private static final String updateStatementString = "UPDATE Client SET Nume = ?, Adresa = ?, Email = ? WHERE Id_Client = ?";

	public static void update(Client client, int Id_Client) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setString(1, client.getNume());
			updateStatement.setString(2, client.getAdresa());
			updateStatement.setString(3, client.getEmail());
			updateStatement.setInt(4, Id_Client);
			updateStatement.executeUpdate();

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}

	}
	
	public static Client findById(int id) {
		Client toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, id);
			rs = findStatement.executeQuery();
			rs.next();

			int Id_Client = rs.getInt("Id_Client");
			String Nume = rs.getString("Nume");
			String Adresa = rs.getString("Adresa");
			String Email = rs.getString("Email");

			toReturn = new Client(Id_Client, Nume, Adresa, Email);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	public static int insert(Client client) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, client.getId_Client());
			insertStatement.setString(2, client.getNume());
			insertStatement.setString(3, client.getAdresa());
			insertStatement.setString(4, client.getEmail());

			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}

	public static void delete(int Id_Client) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;

		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, Id_Client);

			deleteStatement.executeUpdate();

			// ResultSet rs = deleteStatement.getGeneratedKeys();

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}

	}

	public static ArrayList<Client> afisareClient(ArrayList<Client> array) {
		Connection dbConnection = ConnectionFactory.getConnection();
		array = new ArrayList<Client>();

		PreparedStatement afisareStatement = null;
		ResultSet rs = null;
		Client cl = null;
		try {

			afisareStatement = dbConnection.prepareStatement(afisStatementString, Statement.RETURN_GENERATED_KEYS);

			rs = afisareStatement.executeQuery();
			while (rs.next()) {

				int Id_Client = rs.getInt("Id_Client");
				String Nume = rs.getString("Nume");
				String Adresa = rs.getString("Adresa");
				String Email = rs.getString("Email");

				cl = new Client(Id_Client, Nume, Adresa, Email);
				System.out.println(cl.getId_Client()+cl.getNume());
				array.add(cl);

			}

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:afisareClient " + e.getMessage());
		} finally {
			ConnectionFactory.close(afisareStatement);
			ConnectionFactory.close(dbConnection);
			ConnectionFactory.close(rs);
		}
		return array;

	}
}
