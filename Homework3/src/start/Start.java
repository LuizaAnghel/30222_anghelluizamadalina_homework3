package start;

import java.sql.SQLException;

import java.util.logging.Logger;

import presentation.View;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class Start {
	protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());

	public static void main(String[] args) throws SQLException {

		/*
		 * Client client = new
		 * Client(12,"Maniu Alexandra","Str.Primaverii Nr.25, Timisoara",
		 * "alexandra@yahoo.com");
		 * 
		 * ClientBLL clientBll = new ClientBLL(); int Id_Client =
		 * clientBll.insertClient(client); if (Id_Client > 0) {
		 * clientBll.findClientById(Id_Client); }
		 * 
		 * 
		 * // Generate error try { clientBll.findClientById(1); } catch
		 * (Exception ex) { LOGGER.log(Level.INFO, ex.getMessage()); }
		 */

		View view = new View();
		view.createView();

	}
}
