CREATE DATABASE Warehouse;

CREATE TABLE Client (
Id_Client INT(10) NOT NULL auto_increment,
Nume VARCHAR(50),
Adresa VARCHAR(50),
Email VARCHAR(20),
PRIMARY KEY (Id_Client)
);

CREATE TABLE Produs (
Id_Produs INT(10) NOT NULL auto_increment,
Denumire VARCHAR(50) NOT NULL,
Pret DOUBLE(10,2),
Cantitate INT(10),
PRIMARY KEY (Id_Produs)
);

CREATE TABLE Comanda (
Id_Comanda INT(10) NOT NULL auto_increment,
Id_Client INT(10) NOT NULL,
Id_Produs INT(10) NOT NULL,
Cantitate INT(10),
Suma_Finala DOUBLE(10,2),
PRIMARY KEY (Id_Comanda),
FOREIGN KEY(Id_Produs) REFERENCES Produs(Id_Produs),
FOREIGN KEY (Id_Client) REFERENCES Client(Id_Client)
);




